
<!DOCTYPE html>
<html>
<head>
    <title></title>
     <script>
        window.mminime_app_url = '{{ $redirect }}';
        window.mminime_web = '{{ $fallbackURL }}';
        window.mminime_window.mminime_timerDelay        = 500;
        window.mminime_window.mminime_processingBuffer  = 2000;
    </script>
</head>
<body>
    <div style="display:none">
        <ul>
            <li>
                APP SCHEME: <a href="{{ $redirect }}"> {{ $redirect }} </a>
            </li>
            <li>
                FALL BACK: <a href="{{ $redirect }}"> {{ $fallbackURL }} </a>
            </li>
        </ul>
        DEBUG <br/>
        <div id="log"></div>
    </div>
    <script type="text/javascript">
        
        var timestamp         = new Date().getTime();
        
        var redirect = function(url) {
                window.location = url;
                log('ts: ' + timestamp + '; redirecting to: ' + url);

        }
        var isPageHidden = function() {
            var browserSpecificProps = {hidden:1, mozHidden:1, msHidden:1, webkitHidden:1};
            for (var p in browserSpecificProps) {
                if(typeof document[p] !== "undefined"){
                    return document[p];
                }
            }
            return false; // actually inconclusive, assuming not
        }
        var elapsedMoreTimeThanTimerSet = function(){
            var elapsed = new Date().getTime() - timestamp;
            // log('elapsed: ' + elapsed);
            return window.mminime_timerDelay + window.mminime_processingBuffer < elapsed;
        }
        var redirectToFallbackIfBrowserStillActive = function() {
            var elapsedMore = elapsedMoreTimeThanTimerSet();
            log('hidden:' + isPageHidden() +'; time: '+ elapsedMore);

            if (document.hasFocus() == true) {
               redirect('{{ $fallbackURL }}');
            }
            if (isPageHidden() || elapsedMore) {
                log('not redirecting');
            }else{
                redirect(window.mminime_web);
            }
        }
        var log = function(msg){
            document.getElementById('log').innerHTML += msg + "<br>";
        }
            
        setTimeout(redirectToFallbackIfBrowserStillActive, window.mminime_timerDelay);
        redirect(window.mminime_app_url);

    </script>
</body>
</html>