<?php

namespace App\Providers;

use App\Models\UrlStatus;
use App\Observers\UrlStatusObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(env('REDIRECT_HTTPS')) {
            \URL::forceScheme('https');
        }
        UrlStatus::observe(UrlStatusObserver::class);
    }
}
