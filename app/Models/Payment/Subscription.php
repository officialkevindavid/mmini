<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed $users
 * @property \Carbon\Carbon $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $id
 * @property mixed $subscription_modules
 */
class Subscription extends Model
{
    use SoftDeletes;

    CONST FREE = 'F', BALLER = 'B', PAY_AS_YOU_GO = 'P';
    CONST ID_FREE = 1, ID_BALLER = 2, ID_PAY_AS_YOU_GO = 3;
    CONST FREE_TEXT = 'Free',BALLER_TEXT = 'Baller', PAG_TEXT = 'Pay as you grow';
    CONST PLAN = 'Plan';
    CONST MONTHLY = 'Monthly', YEARLY = 'Annually';
    CONST MONTH = 'Month', YEAR = 'Year';
    const ACTIVE = 1, IN_ACTIVE = 0;
    const PLAN_FREE = 0, PLAN_BALLER = 1, PLAN_PAG = 2;
    const BALLER_MONTHLY = "Baller-Monthly", BALLER_YEARLY = "Baller-Annually";
    const YES = 1, NO = 0;
    protected $table = 'subscriptions';
    protected $guarded = ['id'];

    public function subscriptionModules()
    {
        return $this->hasMany(SubscriptionModule::class);
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
