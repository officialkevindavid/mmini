<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UrlStatus extends Model
{
    use SoftDeletes;

    const FIRST_URL = 1, SECOND_URL = 2;
    const SUCCESS = 1, FAIL = 0;

    protected $fillable = [
        'ip',
        'short_url_id',
        'country_code',
        'zip',
        'extra_data',
        'status',
        'status_count',
        'link_id',
        'refer_url',
        'mobile',
        'tablet',
        'phone',
        'ios',
        'android',
        'desktop',
        'created_at_utc',
    ];
}
