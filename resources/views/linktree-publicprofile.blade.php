<!doctype html>
<html lang="en">

<head>
    <link rel="icon" href="{{ asset('images/logo1.png') }}" type="image/png">
    <title>MarketerMagic™ - Marketing Made Easy</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/manyleads.css') }}">

    <link rel="stylesheet" href="{{ asset('css/wm-style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/wm-responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/urlshortner.css') }}">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.7.2/js/all.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <title>MarketerMagic™ - Marketing Made Easy</title>
    <style>
        body {
            position: relative;
            margin: 0;
            max-width: 100%;
            background-color: #fff;
            min-height: 100vh;
            padding: 0px 0 60px !important;
        }

    </style>
</head>
<div class="wrapper">
    <div class="publicprofile">
        <div>
            <div class="publicprofile-banner"></div>
            <div class="text-center mb-5">
            <img src="{{ $linkTreeDatas->user->profile_picture != null ? $linkTreeDatas->user->profile_picture : asset('images/userimg.png') }}" class="img-fluid user-img" alt="">
            <a href="#" class="username color-grey text-capitalize">{{ '@'.$name }}</a>
        </div>
        <div class="publicprofile-links text-center my-4">
            @foreach($linkTreeDatas -> linkTreeUrls as $linkTreeData)
                <div class="link">
                    <a target="_blank" href="{{ env('MMINI_URL','https://mmini.me'). '/' . $linkTreeData->shortUrlTree->short_url }}">{{ $linkTreeData->name }}</a>
                </div>
            @endforeach
        </div>
        <div class="publicpofile-footer text-center">
            <a href="#"><img src="{{ asset('images/full-logo-black.svg') }}" class="img-fluid" alt=""></a>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>

</html>