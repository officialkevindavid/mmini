<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LinkTreeUrl extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $table='link_tree_urls';
    protected $guarded = ['id'];

    public function linkTree()
    {
        return $this->belongsTo(LinkTree::class);
    }
    public function shortUrlTree(){
        return $this->belongsTo(ShortUrl::class,'short_url_id','id');
    }
}
