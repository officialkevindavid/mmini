<?php

namespace App\Models\MMService;

use Illuminate\Database\Eloquent\Model;

class Automation extends Model
{
    protected $guarded = ['automation_id', 'created_at', 'updated_at'];

    protected $hidden = [];

    protected $table = 'textify_automation';

    protected $primaryKey = 'automation_id';

    protected $connection= 'mysql_mmservice';

    /**
     * Get the Web Content associated with the Automation.
     */
    public function Content()
    {
        return $this->hasOne('App\Models\MMService\AutomationContent', 'automation_id', 'automation_id');
    }

    /**
     * Get the Source Code associated with the Automation.
     */
    public function SourceCode()
    {
        return $this->hasOne('App\Models\MMService\AutomationSourceCode', 'automation_id', 'automation_id');
    }

    /**
     * Get the Subscribers for the Automation.
     */
    public function Subscribers()
    {
        return $this->hasMany('App\Models\MMService\AutomationSubscriber', 'automation_id', 'automation_id');
    }

    /**
     * Get the BroadCast for the Automation.
     */
    public function BroadCast()
    {
        return $this->hasMany('App\Models\MMService\BroadCast', 'automation_id', 'automation_id');
    }
}
