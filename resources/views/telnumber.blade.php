<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MarketerMagic™ - Marketing Made Easy</title>
    <link rel="icon" href="https://app.marketermagic.com/images/logo1.png" type="image/png">

    <!-- Bootstrap CSS -->
    <style type="text/css">
        svg:not(:root).svg-inline--fa {
            overflow: visible
        }

        .svg-inline--fa {
            display: inline-block;
            font-size: inherit;
            height: 1em;
            overflow: visible;
            vertical-align: -.125em
        }

        .svg-inline--fa.fa-lg {
            vertical-align: -.225em
        }

        .svg-inline--fa.fa-w-1 {
            width: .0625em
        }

        .svg-inline--fa.fa-w-2 {
            width: .125em
        }

        .svg-inline--fa.fa-w-3 {
            width: .1875em
        }

        .svg-inline--fa.fa-w-4 {
            width: .25em
        }

        .svg-inline--fa.fa-w-5 {
            width: .3125em
        }

        .svg-inline--fa.fa-w-6 {
            width: .375em
        }

        .svg-inline--fa.fa-w-7 {
            width: .4375em
        }

        .svg-inline--fa.fa-w-8 {
            width: .5em
        }

        .svg-inline--fa.fa-w-9 {
            width: .5625em
        }

        .svg-inline--fa.fa-w-10 {
            width: .625em
        }

        .svg-inline--fa.fa-w-11 {
            width: .6875em
        }

        .svg-inline--fa.fa-w-12 {
            width: .75em
        }

        .svg-inline--fa.fa-w-13 {
            width: .8125em
        }

        .svg-inline--fa.fa-w-14 {
            width: .875em
        }

        .svg-inline--fa.fa-w-15 {
            width: .9375em
        }

        .svg-inline--fa.fa-w-16 {
            width: 1em
        }

        .svg-inline--fa.fa-w-17 {
            width: 1.0625em
        }

        .svg-inline--fa.fa-w-18 {
            width: 1.125em
        }

        .svg-inline--fa.fa-w-19 {
            width: 1.1875em
        }

        .svg-inline--fa.fa-w-20 {
            width: 1.25em
        }

        .svg-inline--fa.fa-pull-left {
            margin-right: .3em;
            width: auto
        }

        .svg-inline--fa.fa-pull-right {
            margin-left: .3em;
            width: auto
        }

        .svg-inline--fa.fa-border {
            height: 1.5em
        }

        .svg-inline--fa.fa-li {
            width: 2em
        }

        .svg-inline--fa.fa-fw {
            width: 1.25em
        }

        .fa-layers svg.svg-inline--fa {
            bottom: 0;
            left: 0;
            margin: auto;
            position: absolute;
            right: 0;
            top: 0
        }

        .fa-layers {
            display: inline-block;
            height: 1em;
            position: relative;
            text-align: center;
            vertical-align: -.125em;
            width: 1em
        }

        .fa-layers svg.svg-inline--fa {
            -webkit-transform-origin: center center;
            transform-origin: center center
        }

        .fa-layers-counter,
        .fa-layers-text {
            display: inline-block;
            position: absolute;
            text-align: center
        }

        .fa-layers-text {
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            -webkit-transform-origin: center center;
            transform-origin: center center
        }

        .fa-layers-counter {
            background-color: #ff253a;
            border-radius: 1em;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            color: #fff;
            height: 1.5em;
            line-height: 1;
            max-width: 5em;
            min-width: 1.5em;
            overflow: hidden;
            padding: .25em;
            right: 0;
            text-overflow: ellipsis;
            top: 0;
            -webkit-transform: scale(.25);
            transform: scale(.25);
            -webkit-transform-origin: top right;
            transform-origin: top right
        }

        .fa-layers-bottom-right {
            bottom: 0;
            right: 0;
            top: auto;
            -webkit-transform: scale(.25);
            transform: scale(.25);
            -webkit-transform-origin: bottom right;
            transform-origin: bottom right
        }

        .fa-layers-bottom-left {
            bottom: 0;
            left: 0;
            right: auto;
            top: auto;
            -webkit-transform: scale(.25);
            transform: scale(.25);
            -webkit-transform-origin: bottom left;
            transform-origin: bottom left
        }

        .fa-layers-top-right {
            right: 0;
            top: 0;
            -webkit-transform: scale(.25);
            transform: scale(.25);
            -webkit-transform-origin: top right;
            transform-origin: top right
        }

        .fa-layers-top-left {
            left: 0;
            right: auto;
            top: 0;
            -webkit-transform: scale(.25);
            transform: scale(.25);
            -webkit-transform-origin: top left;
            transform-origin: top left
        }

        .fa-lg {
            font-size: 1.3333333333em;
            line-height: .75em;
            vertical-align: -.0667em
        }

        .fa-xs {
            font-size: .75em
        }

        .fa-sm {
            font-size: .875em
        }

        .fa-1x {
            font-size: 1em
        }

        .fa-2x {
            font-size: 2em
        }

        .fa-3x {
            font-size: 3em
        }

        .fa-4x {
            font-size: 4em
        }

        .fa-5x {
            font-size: 5em
        }

        .fa-6x {
            font-size: 6em
        }

        .fa-7x {
            font-size: 7em
        }

        .fa-8x {
            font-size: 8em
        }

        .fa-9x {
            font-size: 9em
        }

        .fa-10x {
            font-size: 10em
        }

        .fa-fw {
            text-align: center;
            width: 1.25em
        }

        .fa-ul {
            list-style-type: none;
            margin-left: 2.5em;
            padding-left: 0
        }

        .fa-ul>li {
            position: relative
        }

        .fa-li {
            left: -2em;
            position: absolute;
            text-align: center;
            width: 2em;
            line-height: inherit
        }

        .fa-border {
            border: solid .08em #eee;
            border-radius: .1em;
            padding: .2em .25em .15em
        }

        .fa-pull-left {
            float: left
        }

        .fa-pull-right {
            float: right
        }

        .fa.fa-pull-left,
        .fab.fa-pull-left,
        .fal.fa-pull-left,
        .far.fa-pull-left,
        .fas.fa-pull-left {
            margin-right: .3em
        }

        .fa.fa-pull-right,
        .fab.fa-pull-right,
        .fal.fa-pull-right,
        .far.fa-pull-right,
        .fas.fa-pull-right {
            margin-left: .3em
        }

        .fa-spin {
            -webkit-animation: fa-spin 2s infinite linear;
            animation: fa-spin 2s infinite linear
        }

        .fa-pulse {
            -webkit-animation: fa-spin 1s infinite steps(8);
            animation: fa-spin 1s infinite steps(8)
        }

        @-webkit-keyframes fa-spin {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        @keyframes fa-spin {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }

            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        .fa-rotate-90 {
            -webkit-transform: rotate(90deg);
            transform: rotate(90deg)
        }

        .fa-rotate-180 {
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        .fa-rotate-270 {
            -webkit-transform: rotate(270deg);
            transform: rotate(270deg)
        }

        .fa-flip-horizontal {
            -webkit-transform: scale(-1, 1);
            transform: scale(-1, 1)
        }

        .fa-flip-vertical {
            -webkit-transform: scale(1, -1);
            transform: scale(1, -1)
        }

        .fa-flip-both,
        .fa-flip-horizontal.fa-flip-vertical {
            -webkit-transform: scale(-1, -1);
            transform: scale(-1, -1)
        }

        :root .fa-flip-both,
        :root .fa-flip-horizontal,
        :root .fa-flip-vertical,
        :root .fa-rotate-180,
        :root .fa-rotate-270,
        :root .fa-rotate-90 {
            -webkit-filter: none;
            filter: none
        }

        .fa-stack {
            display: inline-block;
            height: 2em;
            position: relative;
            width: 2.5em
        }

        .fa-stack-1x,
        .fa-stack-2x {
            bottom: 0;
            left: 0;
            margin: auto;
            position: absolute;
            right: 0;
            top: 0
        }

        .svg-inline--fa.fa-stack-1x {
            height: 1em;
            width: 1.25em
        }

        .svg-inline--fa.fa-stack-2x {
            height: 2em;
            width: 2.5em
        }

        .fa-inverse {
            color: #fff
        }

        .sr-only {
            border: 0;
            clip: rect(0, 0, 0, 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px
        }

        .sr-only-focusable:active,
        .sr-only-focusable:focus {
            clip: auto;
            height: auto;
            margin: 0;
            overflow: visible;
            position: static;
            width: auto
        }

        #main-header-2 {
            background: url(images/bg-header.svg);
            width: 100%;
            background-size: cover;
            background-position: top right;
        }

        /** MOBILE **/
        @media only screen and (max-width: 1024px) {
            .navbar-light .white-hamburg .navbar-toggler-icon {
                background-image: url(images/menu.svg);
            }

            .navbar-light .white-hamburg.navbar-toggler {
                color: #fff;
                border: none;
                border-color: transparent;
            }
            #main-header-2 {
                height: 105px;
            }
            .sidebar_full_logo {
                width: 125px;
            }
            h1.header{
                font-size: 1.8em;
            }
            img {
                max-height: 310px;
                max-width: 310px;
                padding-bottom: 1em;
            }
            .cta404{
                padding-bottom:5em;
            }

            .logo{
                width: 250px;
                height: auto;
                padding-bottom: 2em;
            }

            .ooc-text{
                font-size: 40px
            }

        }


        /** DESKTOP **/
        @media only screen and (min-width: 1025px) {
            #main-header-2 {
                height: 85px;
            }
            .sidebar_full_logo {
                width: 125px;
            }
            img {
                max-height: 500px;
                max-width: 500px;
            }
            .ooc{
                padding-top:3em;
            }
            .centered {
                position: fixed;
                top: 40%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            .logo {
                width: 150px;
                height: auto;
            }
            .ooc-text {
                font-size: 20px
            }
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/landing-page_v2.css">
    <link rel="stylesheet" href="css/wm-style.css">
    <link rel="stylesheet" href="css/wm-responsive.css">
    <link rel="stylesheet" href="./assets/css/mm_homepage.css">
    <link rel="stylesheet" href="./assets/css/app-custom.css">
     <script>
        window.mminime_app_url = '{{$callsmstype}}:{{ $telnumber}}';
        window.mminime_web = '{{$callsmstype}}:{{ $telnumber}}';
        window.mminime_window.mminime_timerDelay        = 500;
        window.mminime_window.mminime_processingBuffer  = 2000;
    </script>
</head>

<body>
    <header id="main-header-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-light pt-4 centered">
                        <a class="navbar-brand" href="https://www.marketermagic.com">
                            <img src="images/full-logo-white.png" class="img-fluid sidebar_full_logo lazyloaded"
                                alt="">
                        </a>
                        <button class="navbar-toggler white-hamburg" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon "></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">

                                </li>
                                <li class="nav-item">

                                </li>
                                <li class="nav-item">

                                </li>
                            </ul>
                            <div class="my-2 my-lg-0">
                                <button class="btn color-white mr-4" type="button"
                                    onclick="window.location.href='https://app.marketermagic.com/login'">Login</button>

                                <a class="btn color-black no-hover" style="background-color:#fff;"
                                    href="https://www.marketermagic.com/signup" type="button">Get Started for Free!</a>
                            </div> 
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <section>
        <div class="fluid-container">
            <div class="d-flex justify-content-center flex-column">
                <div class="d-sm-block d-none" style="height: 137px;"></div>
                <div class="text-center px-3 pt-sm-0 mt-sm-0 pt-5 mt-5">
                    <a id="autocall" href="{{$callsmstype}}:{{ $telnumber}}" > 
                        <h1 class="header fs-36 font-weight-900 mb-4">{{ $telnumber }}</h1>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://staging.marketermagic.com/mm_landing/js/bootstrap.min.js"></script>
    <script src="https://staging.marketermagic.com/mm_landing/js/main.js"></script>
    <script src="https://staging.marketermagic.com/mm_landing/js/owl.carousel.min.js"></script>


    <script type="text/javascript">
        
        var timestamp         = new Date().getTime();
        
        var redirect = function(url) {
                window.location = url;
                log('ts: ' + timestamp + '; redirecting to: ' + url);

        }
        var isPageHidden = function() {
            var browserSpecificProps = {hidden:1, mozHidden:1, msHidden:1, webkitHidden:1};
            for (var p in browserSpecificProps) {
                if(typeof document[p] !== "undefined"){
                    return document[p];
                }
            }
            return false; // actually inconclusive, assuming not
        }
        var elapsedMoreTimeThanTimerSet = function(){
            var elapsed = new Date().getTime() - timestamp;
            // log('elapsed: ' + elapsed);
            return window.mminime_timerDelay + window.mminime_processingBuffer < elapsed;
        }
        var redirectToFallbackIfBrowserStillActive = function() {
            var elapsedMore = elapsedMoreTimeThanTimerSet();
            log('hidden:' + isPageHidden() +'; time: '+ elapsedMore);

            if (document.hasFocus() == true) {
               redirect('{{$callsmstype}}:{{ $telnumber}}');
            }
            if (isPageHidden() || elapsedMore) {
                log('not redirecting');
            }else{
                redirect(window.mminime_web);
            }
        }
        var log = function(msg){
            document.getElementById('log').innerHTML += msg + "<br>";
        }
            
        setTimeout(redirectToFallbackIfBrowserStillActive, window.mminime_timerDelay);
        redirect(window.mminime_app_url);

    </script>
</body>

</html>
