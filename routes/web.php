<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ShortUrlController@welcome');
Route::get('AAPBkWWQUY', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/@{url}', 'ShortUrlController@linkTreeProfile')->name('shortUrl.linkTreeProfile');
Route::get('/out-of-credit', 'ShortUrlController@outofcredit')->name('outofcredits');
Route::get('/404', 'ShortUrlController@fourzerofour')->name('404');
Route::get('/{shortUrl}', 'ShortUrlController@index');
Route::get('/deep/{shortUrl}', 'ShortUrlController@deepLink');

//test route

Route::get('test/test', 'ShortUrlController@usess');
Route::get('/linkTreeProfile/@{url}', 'ShortUrlController@linkTreeProfile')->name('shortUrl.linkTreeProfile');
