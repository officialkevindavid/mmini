<?php


namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class RedirectLog extends Model
{
    protected $collection = 'redirect_log';
    protected $connection = 'mongodb';

    protected $fillable = [ 
                    'short_url',
                    'redirect_url',
                    'type',
                    'user_id',
                    'referer',
                    'ip_address',
                    'country',   
                ];


}