$(document).ready(function() {

    $('#recent-activity-preview').on('click', function() {
        $('.RecentActivityPopup').addClass("d-block");
        window.setTimeout(function(){
            $('.RecentActivityPopup').removeClass("d-block");
    }, 6000);
    });

    $('#recent-activity-close').on('click', function() {
        $('.RecentActivityPopup').removeClass("d-block");
    });

    $('#live-visitor-preview').on('click', function() {
        $('.LiveVisitorPopup').addClass("d-block");
        window.setTimeout(function(){
            $('.LiveVisitorPopup').removeClass("d-block");
    }, 6000);
    });

    $('#live-visitor-close').on('click', function() {
        $('.LiveVisitorPopup').removeClass("d-block");
    });

    $('#custom-message-preview').on('click', function() {
        $('.CustomMessagePopup').addClass("d-block");
        window.setTimeout(function(){
            $('.CustomMessagePopup').removeClass("d-block");
    }, 6000);
    });

    $('#custom-message-close').on('click', function() {
        $('.CustomMessagePopup').removeClass("d-block");
    });

    $('#editme-button').on('click', function() {
        $('.editme-input').addClass("d-block");
        $('.editme-text').addClass("d-none");
    });


    $("#sidebar").hover(
        function() {
            $(this).removeClass("active");
        },
        function() {
            $(this).addClass("active");
        }
    );

    $('#editname-button').on('click', function() {
        $(this).addClass('d-none');
        $('.editname-input').addClass("d-inline");

        $('#closename-button').addClass("d-inline");
        $('#savename-button').addClass("d-inline");
        $('.edit-name').addClass("d-none");

    });

    $('#savename-button,#closename-button').on('click',function(){
        $('#savename-button').removeClass('d-inline');
        $('#closename-button').removeClass("d-inline");
        $('.editname-input').removeClass("d-inline");
        $('.edit-name').removeClass("d-none");
        $('#editname-button').removeClass('d-none');
    });

});