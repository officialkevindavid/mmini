<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Telnumber extends Model
{
    protected $table = 'shorturl_telnumbers';
    
    use SoftDeletes;
    //shorturl_telnumbers
    protected $fillable = [
        'id',
        'short_url_id',
        'default_type',
        'advanced',
        'android',
        'ios',
    ];

    public function ShortUrl(){
        return $this->hasOne(Telnumber::class);
    }
}