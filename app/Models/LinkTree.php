<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LinkTree extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $table = 'link_trees';
    protected $fillable = [
        'id',
        'user_id',
        'unique_name'
    ];

    public function linkTreeUrls()
    {
        return $this->hasMany(LinkTreeUrl::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
