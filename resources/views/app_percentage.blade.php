@extends('emails.emailformat_worksummary')

@section('content')
    <style>

        td, h3 {
            padding: 3px;
        }
        .linear-btn {
            font-style: normal;
            font-weight: 500;
            line-height: 22px;
            font-size: 10px;
            color: #FFFFFF;
            background: #02CED1;
            border-radius: 3px;
            padding: 12px 20px;
            margin-right: 10px;
            text-decoration: none;
            text-transform: uppercase;
        }
        .tcolor{
            color: #000000!important;
        }
        @media (max-width: 576px) {
            .mobile-none {
                display: none !important;
            }
            .mobile-w100 {
                width: 100% !important;
            }
            .linear-btn {
                padding: 10px 15px;
                display:inline-block;
            }
            .display-block-mobile {
                display: block !important;
                margin-top: 10px;
            }
        }
    </style>

    <div style="background-color: #fff">
        <table style="background: #fff;border-radius: 3px;    padding-bottom: 2%;" width="100%" cellpadding="0"
               cellspacing="0">
            <tr>
                <td style="vertical-align: top;width: 25%;">&nbsp;</td>
                <td style="vertical-align: top;width: 50%;">&nbsp;</td>
                <td style="vertical-align: top;width: 25%;">&nbsp;</td>
            </tr>
            <tr>
                <td style="vertical-align: top;width: 100%;" class="mobile-w100">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:20%;text-align:center;">
                                <img src="{{ asset('images/emailformat-work.png') }}">
                            </td>
                            <td style="width:100%;">
                                <h3 class="ma-0"
                                    style="margin-top: 20px;font-family:Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;color: #000;margin: 40px 0 0;line-height: 1.2;font-weight: 550;">
                                    Hi {{ $name }},</h3>
                                <div class="ma-0"
                                     style="margin-top: 20px;color: #000;margin: 20px 0 0;line-height: 1.2;font-size: 16px;">
                                    Your {{ $plan }} credits for {{ $appName}} are almost expired - to prevent a service
                                    interruption and lost customers click the button below to immediately upgrade your
                                    plan so you don’t miss out!</div>
                                <div style="text-align: left;margin: 14px 0;">
                                    <a href = "{{ route('subscribe.index', ['r' => 'p']) }}">Click HERE</a>
                                    <span style="font-size: 12px;color: #000000!important;" class="display-block-mobile tcolor">&nbsp;to Upgrade Now!</span>
                                </div>
                                <div style="text-align: left;color: #000000!important;" class="tcolor">Remember We Have a 30 Day Money Back Guarantee - So There is No Risk!
                                    <p>See You Soon</p>
                                    <p>Kevin</p>
                                    <p>Marketing Magician :)</p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;width: 20%;" class="mobile-none">&nbsp;</td>
            </tr>
        </table>
        <hr style="margin-left: 20%;margin-right: 20%;height: 1px;color: #e2d6d6;background-color: #e2d6d6;border: none;"/>
        <table style="background: #fff;border-radius: 3px;" width="100%" cellpadding="0"
               cellspacing="0">
            <tr>
                <td style="vertical-align: top;width: 25%;" class="mobile-none"></td>
                <td style="vertical-align: top;width: 50%;" class="mobile-w100">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:33%;text-align:center;">
                                <h3 style="font-family: Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;color: #000;margin: 0px 0 0;line-height: 1.2;font-size: 24px;">{{ $appName}}</h3>
                            </td>
                            <td style="width:33%;text-align:center;">
                                <h3 style="font-family: Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;color: #000;margin: 0px 0 0;line-height: 1.2;font-size: 24px;">{{ $percentage }}
                                    % Used</h3>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;width: 25%;" class="mobile-none">&nbsp;</td>
            </tr>
        </table>
        <hr style="margin-left: 20%;margin-right: 20%;height: 1px;color: #e2d6d6;background-color: #e2d6d6;border: none;"/>
        <h3 style="font-family: Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;color: #000;margin: 10px 0;line-height: 1.2;font-size: 24px;text-align: center">{{ $used }}
            out of {{ $total }} credits used.</h3>
    </div>
@endsection