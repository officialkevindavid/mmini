<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeepLink extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id',
        'short_url_id',
        'default_url',
        'android_scheme',
        'ios_scheme',
    ];

    public function ShortUrl(){
        return $this->hasOne(DeepLink::class);
    }
}