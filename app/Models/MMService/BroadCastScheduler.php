<?php

namespace App\Models\MMService;

use Illuminate\Database\Eloquent\Model;

class BroadCastScheduler extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [];

    protected $table = 'textify_broadcast_scheduled_status';

    protected $primaryKey = 'id';

    protected $connection = 'mysql_mmservice';

    /**
     * Get the BroadCast that owns the Scheduler.
     */
    public function BroadCast()
    {
        return $this->belongsTo('App\Models\MMService\BroadCast', 'broadcast_id', 'broadcast_id');
    }
}
