<!DOCTYPE html>
<html>
    <head>
        <title>Out of Credit</title>
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@700&display=swap" rel="stylesheet">
        <meta name="description" content="The Link owner lacks credit to redirect you properly."/>
        <style>
            .centered {
                position: fixed;
                top: 40%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            p{
                text-align: center;
                font-weight: 700;
            }

            body{
                font-family: 'Nunito Sans', sans-serif;
            }

            /** MOBILE **/
            @media only screen and (max-width: 1024px) {
                img {
                    max-height: 600px;
                    max-width: 600px;
                }
                .ooc{
                    padding-top:5em;
                }

                .logo{
                    width: 250px;
                    height: auto;
                }

                .ooc-text{
                    font-size: 40px
                }

            }

            /** DESKTOP **/
            @media only screen and (min-width: 1025px) {
                img {
                    max-height: 350px;
                    max-width: 350px;
                }
                .ooc{
                    padding-top:3em;
                }
                .centered {
                    position: fixed;
                    top: 40%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                }
                .logo {
                    width: 150px;
                    height: auto;
                }
                .ooc-text {
                    font-size: 20px
                }
            }
        </style>
    </head>
    <body>
        <div class="container centered">
            <p>
                <a href="https://marketermagic.com">
                    <img class="logo" src="images/logo2.png">
                </a>
            </p>
            <p>
                <img class="ooc" src="images/out_of_credits.png">
            </p>
            <p class="ooc-text">
                Minime - Out of credits
            </p>
        </div>
    </body>
</html>