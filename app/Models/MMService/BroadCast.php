<?php

namespace App\Models\MMService;

use Illuminate\Database\Eloquent\Model;

class BroadCast extends Model
{
    const PICSIZE = 1000000;
    const PICWIDTH = 1080;

    protected $guarded = ['broadcast_id', 'created_at', 'updated_at'];

    protected $hidden = [];

    protected $table = 'textify_broadcast';

    protected $primaryKey = 'broadcast_id';

    protected $connection = 'mysql_mmservice';

    /**
     * Get the BroadCastScheduler for the BroadCast.
     */
    public function BroadCastScheduler()
    {
        return $this->hasMany('App\Models\MMService\BroadCastScheduler', 'broadcast_id', 'broadcast_id');
    }

    /**
     * Get the Automation that owns the BroadCast.
     */
    public function Automation()
    {
        return $this->belongsTo('App\Models\MMService\Automation', 'automation_id', 'automation_id');
    }

    /**
     * Get the Subscribers for the BroadCast with the ref of Automation Or User.
     */
    public function Subscribers()
    {
        if ($this->automation_id) {
            return $this->hasMany('App\Models\MMService\AutomationSubscriber', 'automation_id', 'automation_id');
        } else {
            return $this->hasMany('App\Models\MMService\AutomationSubscriber', 'user_id', 'user_id');
        }
    }
}
