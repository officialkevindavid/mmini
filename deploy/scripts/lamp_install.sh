#!/bin/bash

#####################################################
##################### Apache ########################
#####################################################

# To install the Apache Web Server, type:
echo "install apache2"
apt-get install -y apache2

#####################################################
###################### PHP ##########################
#####################################################

# To install PHP, type:

sudo apt-get update
sudo apt -y install software-properties-common
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update

echo "install PHP 7.4 and plugins"
apt-get install -y php7.4 php7.4-cli php7.4-gd php7.4-mongodb php7.4-xml php7.4-xmlrpc php7.4-json php7.4-mysql php7.4-curl php7.4-zip php7.4-mbstring php7.4-imagick

#####################################################
##################### MySQL #########################
#####################################################
# Install mysql-client version 5.7
echo "install mysql-client"
apt-get install -y mysql-client

#####################################################
###### Modify some of the default settings ##########
#####################################################

sed -i "s/memory_limit = 128M/memory_limit = 256M/g" /etc/php/7.4/apache2/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 100M/g" /etc/php/7.4/apache2/php.ini
sed -i "s/post_max_size = 8M/post_max_size = 100M/g" /etc/php/7.4/apache2/php.ini
sed -i "s/; max_input_vars = 1000/max_input_vars = 1000/g" /etc/php/7.4/apache2/php.ini

#####################################################
############### Create directory ####################
#####################################################

# Create directory /var/www/html/mmini
mkdir -p /var/www/html/mmini
# Change ownership of /var/www/html/mmini directory
chown www-data:www-data -R /var/www/html/mmini
usermod -a -G www-data ubuntu
# Start the Apache Web Server on boot:
echo "enable apache2"
systemctl enable apache2
# Start the Apache Web Server:
echo "start apache2"
systemctl start apache2
