<?php

namespace App\Models\MMService;

use Illuminate\Database\Eloquent\Model;

class AutomationSubscriber extends Model
{
    const CSVSIZE = 20971520;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [];

    protected $table = 'textify_automation_subscribers';

    protected $primaryKey = 'id';

    protected $connection= 'mysql_mmservice';

    /**
     * Get the Automation that owns the Subscriber.
     */
    public function Automation()
    {
        return $this->belongsTo('App\Models\MMService\Automation', 'automation_id', 'automation_id');
    }
}
