<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSubscription extends Model
{
    use SoftDeletes;

    CONST SUBSCRIBED = 1, NOT_SUBSCRIBED = 0, EXPIRED = 0, NOT_EXPIRED = 1, ACTIVE = 1, IN_ACTIVE = 0;

    protected $table = 'user_subscriptions';
    protected $guarded = ['id'];

    public function subscriptionModule()
    {
        return $this->belongsTo(SubscriptionModule::class);
    }

    public function scopeCurrentUser($query)
    {
        return $query->where('user_id', auth()->id());
    }

    public function scopeActiveSubscription($query)
    {
        return $query->where('status', UserSubscription::ACTIVE);
    }

    public function scopeIsSubscribed($query)
    {
        return $query->where('is_subscribed', UserSubscription::SUBSCRIBED);
    }
}
