#!/bin/bash

# Init custom vars
export deployPath=/var/www/html/mmini
chmod +x $deployPath/deploy/config/.vars.$DEPLOYMENT_GROUP_NAME
. $deployPath/deploy/config/.vars.$DEPLOYMENT_GROUP_NAME

#####################################################
############ Apache  Configuration ##################
#####################################################

echo "disable 000-default site"
a2dissite 000-default

echo "<VirtualHost *:80>
    ServerAdmin dennis.henkes@eaziit.com
    ServerName $serverName
	
    DocumentRoot $deployPath
    <Directory $deployPath>
        Options Indexes FollowSymLinks
        AllowOverride All
        Order allow,deny
        allow from all
        Require all granted
    </Directory>
	
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<IfModule mpm_prefork_module>
       StartServers            10
       MinSpareServers         10
       MaxSpareServers         35
       MaxRequestWorkers       50
       MaxConnectionsPerChild 800
</IfModule>" > /etc/apache2/sites-available/mmini.conf

a2ensite mmini
a2enmod rewrite
systemctl restart apache2

#####################################################
########### Install Lavarel Application #############
#####################################################
echo "move $deployPath/deploy/config/.env.$DEPLOYMENT_GROUP_NAME to $deployPath/.env"
mv $deployPath/deploy/config/.env.$DEPLOYMENT_GROUP_NAME $deployPath/.env

echo "install composer"

export HOME=/root
export COMPOSER_HOME=/root
cd /root

curl -sS https://getcomposer.org/installer -o composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin --filename=composer

echo "install php dependencies and execute artisan commands"
cd $deployPath 
echo "composer install.."
composer install

echo "php artisan key:generate --force"
php artisan key:generate --force

echo "php artisan migrate"
php artisan migrate

mkdir -p $deployPath/storage/framework/cache/data
chown -R www-data:www-data $deployPath
chmod -R ug+rwx $deployPath/storage $deployPath/bootstrap/cache

php artisan cache:clear
php artisan config:clear
php artisan route:clear

systemctl restart apache2
