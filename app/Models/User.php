<?php

namespace App\Models;

use App\Models\Subscription;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

/**
 * @property mixed $subscription
 */
class User extends Authenticatable
{

    use SoftDeletes, Notifiable;
    CONST MASTER = 'Master';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get a validator for user.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
//            'phone'=> 'required',
            'email' => 'required_without:id|email|max:255|unique:users' . ($id != '' ? ',email,' . $id : ''),
//            'password' => ($id == '' ? 'required|confirmed|min:6' : ''),
//            'role_id' => 'required',

        ]);
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function subscription()
    {
        return $this->belongsTo(\App\Models\Payment\Subscription::class);
    }

    protected function checkEmailUser($email)
    {
        $result = DB::table('users')->where(array('email' => $email))->first();
        return $result;
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function can($permissions, $arguments = [])
    {
        return in_array($permissions, \Cache::get('user-permissions'), TRUE);
    }

    public function wallets()
    {
        return $this->hasMany(UserWallet::class);
    }
}
