<?php

namespace App\Models\MMService;

use Illuminate\Database\Eloquent\Model;

class AutomationContent extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [];

    protected $table = 'textify_automation_web_content';

    protected $primaryKey = 'id';

    protected $connection= 'mysql_mmservice';

    /**
     * Get the Automation that owns the web content.
     */
    public function Automation()
    {
        return $this->belongsTo('App\Models\MMService\Automation', 'automation_id', 'automation_id');
    }
}
