<?php

namespace App\Models\MMService;

use Illuminate\Database\Eloquent\Model;

class AutomationSourceCode extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $hidden = [];

    protected $table = 'textify_automation_source_code';

    protected $primaryKey = 'id';

    protected $connection= 'mysql_mmservice';

    /**
     * Get the Automation that owns the Source Code.
     */
    public function Automation()
    {
        return $this->belongsTo('App\Models\MMService\Automation', 'automation_id', 'automation_id');
    }
}
