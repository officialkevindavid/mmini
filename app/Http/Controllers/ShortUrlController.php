<?php

namespace App\Http\Controllers;

use App\Models\AutomationSubscriber;
use App\Models\DeepLink;
use App\Models\LinkTree;
use App\Models\MMService\Automation;
use App\Models\Payment\UserSubscription;
use App\Models\Role;
use App\Models\ShortUrl;
use App\Models\UrlStatus;
use App\Models\RedirectLog;

use App\Models\User;
use App\Models\UserWallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;


class ShortUrlController extends Controller
{
    public function welcome()
    {
        return redirect()->route('404');
//        return view('welcome');
    }

    public function outofcredit()
    {
        return view("outofcredit");
//        return view('welcome');
    }

    public function fourzerofour()
    {
        return view("404");
    }

    //check uses for user
    public function uses($userId)
    {
        $records = ShortUrl::whereUserId($userId)
            ->where('status.status', '=', '1')
            ->join('url_statuses as status', 'status.short_url_id', '=', 'short_urls.id', 'left outer')
            ->withTrashed()
            ->get();
        $count = count($records);
        if ($count > 500) {
            // subscription end or not
            return redirect()->route('404');
        } else {
            return;
        }
    }

    public function linkTreeProfile($id)
    {
        $linkTreeData = LinkTree::where('unique_name', $id)->with('linkTreeUrls.shortUrlTree', 'user')->first();
        if ($linkTreeData == null) {
            $this->logRedirections($id, url('404'));
            return redirect()->route('404');
        }

        $this->logRedirections($id, url("/@{$id}"), 4, $linkTreeData->user->id);
        return view('linktree-publicprofile', ['linkTreeDatas' => $linkTreeData, 'name' => $id]);
    }

    public function index($data)
    {  
        if($data == 'information'){
            return 'https://marketermatic.com';
        }

        $linkType = ShortUrl::whereRaw("BINARY `short_url`= ?",[$data])->first();
        if (!isset($linkType)) {
            $this->logRedirections($data, url('404'));
            Log::info("mminime no data: {$data} -> 404");
            return redirect()->route('404');
        }
        $exception_url = $linkType->long_url_first;

        $referUrl = [];
        if (isset($_SERVER['HTTP_REFERER'])) {
            $referUrl = $_SERVER['HTTP_REFERER'];
            $urlParts = parse_url($referUrl);
            $referUrl = preg_replace('/^www\./', '', $urlParts);
        } else {
            $referUrl['host'] = 'Direct';
        }

        // build URL array
        $url = [
            "url" => $linkType->long_url_first,
            "link_id" => 1,
            "short_url_id" => $linkType->id,
            "default_url" => "",
            "type" => $linkType->type,
        ];
        $exception_url = $url['url'];

        // change url array if link type is LINK SPLIT type = 1
        if ($linkType->type == ShortUrl::SPLITURL) {
            $url = $this->percentage($data);
            $exception_url = $url['url'];
        } 

        // get Client IP Address
        $clientIP = $this->getUserIP();
        if ($clientIP['fail'] == 'fail') {
            $this->Short_url_log(
                [
                    'short_url_id' => $url['short_url_id'],
                    'ip' => 'fail',
                    'status' => '1',
                    'country_code' => '',
                    'zip' => '',
                    'extra_data' => 'fail',
                    'link_id' => '1',
                    'refer_url' => $referUrl['host']
                ]
            );
            return view('exception_page', ['url' => $exception_url]);
        }

        // retargetting link
        // show pixel html before redirecting | type = 2
        if ($linkType->type == ShortUrl::RETARGETING) {
            $query = DB::select('select * from 
                                short_urls AS su 
                                LEFT join utms AS u 
                                ON su.id = u.short_url_id 
                                INNER JOIN pixel_codes AS pc 
                                ON FIND_IN_SET(pc.id, u.pixel_id) > 0
                                where short_url_id = ' . $url['short_url_id']);
            if (count($query)) {
                if ($this->useWallet($linkType->user_id) == true) {
                    $this->Short_url_log(
                        [
                            'short_url_id' => $url['short_url_id'],
                            'ip' => $clientIP['query'],
                            'status' => 1,
                            'country_code' => $clientIP['countryCode'],
                            'zip' => $clientIP['zip'],
                            'extra_data' => json_encode($clientIP),
                            'link_id' => $url['link_id'],
                            'refer_url' => $referUrl['host']
                        ]
                    );
                    $this->callPixel($query, $url);
                } else {
                    Log::info("mminime no wallet link type 2: {$data} -> out of credit page");
                    $this->logRedirections($data, url('outofcredits'), $linkType->type, $linkType->user_id);
                    return redirect()->route('outofcredits');
                }
            }
        }

        // use wallet for type link split, deeplink and, retargetting
        $deductCreditTypes = [ShortUrl::SPLITURL, ShortUrl::DEEPLINK,  ShortUrl::TELNUMBER];
        if (in_array($linkType->type, $deductCreditTypes)) {
            if ($this->useWallet($linkType->user_id) == false) {
                Log::info("mminime no wallet link type {$linkType->type}: {$data} -> out of credit page");
                $this->logRedirections($data, url('outofcredits'), $linkType->type, $linkType->user_id);
                return redirect()->route('outofcredits');
            }
        } 


        // default minime behaviour 
        // log redirect
        $this->Short_url_log(
            [
                'short_url_id' => $url['short_url_id'],
                'ip'           => $clientIP['query'],
                'status'       => 1,
                'country_code' => $clientIP['countryCode'],
                'zip'          => $clientIP['zip'],
                'extra_data'   => json_encode($clientIP),
                'link_id'      => $url['link_id'],
                'refer_url'    => $referUrl['host']
            ]
        );

        // deeplink or type = 3
        if ($linkType->type == ShortUrl::DEEPLINK) {
            return $this->deepLink($data);
        }

        // telnumber or type = 5
        if ($linkType->type == ShortUrl::TELNUMBER) {
            return $this->telnumber($linkType);
        }

        // regular minime link
        // type = 0
        $redirectURL = (strpos($url['url'], 'http') !== false) ? $url['url'] : 'http://' . $url['url'];
        Log::info("mminime type {$linkType->type}: {$data} -> {$redirectURL}");
        $this->logRedirections($data, $redirectURL, $linkType->type, $linkType->user_id);
        return redirect($redirectURL);
    
        
    }

    public function logRedirections($backhalf, $redirectURL, $redirectTypeInt = 0, $user_id = NULL)
    {
        $clientIP = $this->getUserIP();
        

        $allTypes =  [ 0 => 'regular' , 1 =>'spliturl', 2 =>'retargeting', 3 =>'deeplink', 4 =>'linktree', 5 =>'telnumber'] ;

        $referer = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER'] : 'Direct';
        $type = isset($allTypes[$redirectTypeInt])? $allTypes[$redirectTypeInt] : 'regular';

        $data = ['short_url'    => $backhalf,
                 'redirect_url' => $redirectURL,
                 'type'         => $type,
                 'user_id'      => $user_id,
                 'referer'      => $referer,
                 'ip_address'   => $clientIP['query'],
                 'country'      => $clientIP['countryCode'],
             ];
        
        try{
            RedirectLog::create($data);
        }catch(\Exception $ex){
            Log::info("ERROR => " . $ex->getMessage());
        }
        return True;
    }

    public function telnumber($shorturl)
    {
        $telnumber_options = $shorturl->telnumber->toArray();

        $callsmstype = $telnumber_options['default_type'];

        $detect = new \Mobile_Detect();
        $mobile = $desktop = $tablet = $phone = $ios = $android = 0;
        // Any mobile device (phones or tablets).
        if ( !$detect->isMobile() ) {
            $desktop = 1;
         
        }

        // Check for a specific platform with the help of the magic methods:
        if( $detect->isiOS() ){
            $ios = 1;
        }
         
        if( $detect->isAndroidOS() ){
            $android = 1;
        }

        // fetch
        if($telnumber_options['advanced'] == 1 && $ios == 1){
            $callsmstype = $telnumber_options['ios'];

        }

        if($telnumber_options['advanced'] == 1 && $android == 1){

            $callsmstype = $telnumber_options['android'];


        }
        
        Log::info("telnumber: {$shorturl->short_url} -> TelNumber Landing Page");
        $this->logRedirections($shorturl->short_url, 'TelNumber Landing Page', 5, $shorturl->user_id);
        // convert "call" from db to tel links
        if($callsmstype == 'call') $callsmstype = 'tel';

        return view('telnumber')->with([
            'telnumber' => $shorturl->long_url_first, 
            'callsmstype' => $callsmstype
        ]);
    }

    //get a redirect url percentage
    public function percentage($data)
    {
        $data = ShortUrl::get()->where('short_url', $data);
        if (count($data) <= 0) {
            $sendUrl['default_url'] = env('DEFAULT_URL', 'http://www.marketermagic.com');         // default url
            return $sendUrl;
        }
        foreach ($data AS $data) {
            $data = $data;
        }
        $percentage = $data['percentage'];
        if ($percentage < 0 || $percentage == null || $percentage == '' || $data['long_url_second'] == null) {
            $percentage = ShortUrl::PERCENTAGE;
        }
        $first_url = $data['long_url_first'];
        $second_url = $data['long_url_second'];
        $short_url_id = $data['id'];

        $call_link = UrlStatus::where('short_url_id', $data['id'])->get();
        $total_call = count($call_link);
        $first_link = 0;
        $second_link = 0;
        foreach ($call_link AS $data) {
            if ($data['link_id'] == '1') {
                $first_link++;
            } else {
                $second_link++;
            }
        }
        $total = $total_call;
        if ($total == 0) {
            $total = 1;
            $first_link = 1;
        }
        $data = array();
        $countPercentage = (($first_link / ($total)) * 100);
        $countPercentage = $countPercentage == 0 ? '1' : $countPercentage;
        if ($countPercentage <= $percentage) {
            $data['url'] = $first_url;
            $data['link_id'] = UrlStatus::FIRST_URL;
            $data['short_url_id'] = $short_url_id;
            $data['default_url'] = '';
        } else {
            $data['url'] = $second_url;
            $data['link_id'] = UrlStatus::SECOND_URL;
            $data['short_url_id'] = $short_url_id;
            $data['default_url'] = '';
        }
        return $data;
    }

    // get a client provider ip and all data of provider
    private function getUserIP()
    {
        $myArr = array();
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        $clientIP = '';

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $clientIP = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $clientIP = $forward;
        } else {
            $clientIP = $remote;
        }

        if (env('APP_ENV') == 'local') {
            $clientIP = '23.201.9.94';
        }


        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $clientIP));
        if ($query && $query['status'] == 'success') {
            $myArr = $query;
            $myArr['fail'] = 'success';
            //$myArr = json_encode($query);
        } else {
            $myArr['fail'] = 'fail';
        }
        return $myArr;
    }

// get link statuss
    public function linkstatus($url)
    {
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);

        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if ($httpCode <= 400 && $httpCode != 0) {
            $status = ShortUrl::SUCCESS;
        } else {
            $status = ShortUrl::FAIL;
        }
        curl_close($handle);
        return $status;
    }

    // database entry in status and data
    function Short_url_log($data)
    {

        $detect = new \Mobile_Detect();
        $mobile = $desktop = $tablet = $phone = $ios = $android = 0;
        // Any mobile device (phones or tablets).
        if ( $detect->isMobile() ) {
            $mobile = 1;
        }

        // Any tablet device.
        if( $detect->isTablet() ){
            $tablet = 1;
        }
         
        // Exclude tablets.
        if( $detect->isMobile() && !$detect->isTablet() ){
            $phone = 1;
        }
         
        // Check for a specific platform with the help of the magic methods:
        if( $detect->isiOS() ){
            $ios = 1;
        }
         
        if( $detect->isAndroidOS() ){
            $android = 1;
        }

        if($mobile === 0){
            $desktop = 1;
        }
        $data = [
                'ip'             => $data['ip'],
                'country_code'   => $data['country_code'],
                'zip'            => $data['zip'],
                'extra_data'     => $data['extra_data'],
                'status'         => $data['status'],
                'status_count'   => 1,
                'short_url_id'   => $data['short_url_id'],
                'link_id'        => $data['link_id'],
                'refer_url'      => $data['refer_url'],
                'mobile'         => $mobile,
                'tablet'         => $tablet,
                'phone'          => $phone,
                'ios'            => $ios,
                'android'        => $android,
                'desktop'        => $desktop,
                'created_at_utc' => \Carbon\Carbon::now('UTC')->format('Y-m-d H:i:s'),
            ];

        return UrlStatus::create($data);
    }

    //call a pixel code
    public function callPixel($data, $url)
    {
        foreach ($data as $query) {
            echo $query->pixel_code;
            /*if ($query->pixel_type == 1) {
                // facebook pixel
                echo "
                        <!-- Facebook Pixel Code -->
                        <script>
                          !function(f,b,e,v,n,t,s)
                          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                          n.queue=[];t=b.createElement(e);t.async=!0;
                          t.src=v;s=b.getElementsByTagName(e)[0];
                          s.parentNode.insertBefore(t,s)}(window, document,'script',
                          'https://connect.facebook.net/en_US/fbevents.js');
                          fbq('init', '$query->pixel_code');
                          fbq('track', 'PageView');
                        </script>
                        <noscript><img height=\"1\" width=\"1\" style=\"display:none\" src=\"https://www.facebook.com/tr?id=$query->pixel_code&ev=PageView&noscript=1\" /></noscript>
                        <!-- End Facebook Pixel Code -->
                    ";
            } else if ($query->pixel_type == 0) {
                // google pixel
                echo "
                        <!-- Global site tag (gtag.js) - Google Analytics -->
                        <script async src=\"https://www.googletagmanager.com/gtag/js?id=$query->pixel_code\"></script>
                        <script>
                          window.dataLayer = window.dataLayer || [];
                          function gtag(){dataLayer.push(arguments);}
                          gtag('js', new Date());
                        
                          gtag('config', '$query->pixel_code');
                        </script>
                    ";
            }*/
        }
        $redirectURL = (strpos($url['url'], 'http') !== false) ? $url['url'] : 'http://' . $url['url'];
        header('Refresh: 0.5; url=' . $redirectURL);
        die();
        return false;
    }

    // detect device
    public function detectDevices()
    {
        //Detect special conditions devices
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");

        //do something with this information
        if ($iPod || $iPhone) {
//            $device = 'ipod/iphone';//browser reported as an iPhone/iPod touch -- do something here
            $device = 'iphone';//browser reported as an iPhone/iPod touch -- do something here
        } else if ($iPad) {
//            $device = 'ipad';//browser reported as an iPad -- do something here
            $device = 'iphone';//browser reported as an iPad -- do something here
        } else if ($Android) {
            $device = 'android';//browser reported as an Android device -- do something here
        } else if ($webOS) {
            $device = 'web';//browser reported as a webOS device -- do something here
        } else {
            $device = 'fail';
        }

        return $device;
    }

    // deep link
    public function deepLink($url)
    {
        // $userId = json_decode(ShortUrl::whereShortUrl($url)->get(), true);

        $shortUrlId = json_decode(ShortUrl::whereShortUrl($url)->first(), true);
        $deepLink = json_decode(DeepLink::whereShortUrlId($shortUrlId['id'])->first(), true);
        $devices = $this->detectDevices();


        $redirectURL = $deepLink['default_url'];

        if ($devices == 'iphone') {
            Log::info("deeplink iphone: {$url} -> {$deepLink['ios_scheme']}");
            $redirectURL = $deepLink['ios_scheme'];
        } else if ($devices == 'android') {
             Log::info("deeplink android: {$url} -> {$deepLink['android_scheme']}");
            $redirectURL = $deepLink['android_scheme'];
        } 

        Log::info("deeplink desktop/other: {$url} -> {$deepLink['default_url']}");
        $fallbackURL = $deepLink['default_url'];
        

        $this->logRedirections($url, $deepLink['default_url'], 3, $shortUrlId['user_id']);

        return view('deeplink-redirect')->with(['redirect' => $redirectURL, 
                                                'fallbackURL' => $deepLink['default_url']]
                                            );
    }

    // add url status
    public function deepLinkStatus($url)
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $referUrl = $_SERVER['HTTP_REFERER'];
            $urlParts = parse_url($referUrl);
            $referUrl = preg_replace('/^www\./', '', $urlParts);
        } else {
            $referUrl['host'] = 'Direct';
        }

        $clientIP = $this->getUserIP();
        if ($clientIP['fail'] == 'fail') {
            $this->logRedirections($url, url('404'));
            return redirect()->route('404');
        }

        $this->Short_url_log(
            [
                'short_url_id' => $url,
                'ip' => $clientIP['query'],
                'status' => '1',
                'country_code' => $clientIP['countryCode'],
                'zip' => $clientIP['zip'],
                'extra_data' => json_encode($clientIP),
                'link_id' => '1',
                'refer_url' => $referUrl['host']
            ]
        );
    }

    public function useWallet($user_id)
    {

        try{

            $user = User::where('id', $user_id)->first();

            if(empty($user)){
                return false;
            }

            $api_token = $user->api_token;

            $params = [ 'count' => 1,
                        'action'=> 'minus',
                        'module_id'=> 6,
                        'api_token'=> $api_token];

            // Create Guzzle client
            $client = new Client();
            $url = config('app.DEFAULT_URL') . '/api/wallet';

            // Send request and collect response
            $response = $client->request('POST', $url, ['query' => $params]);

            // process json body
            $body = $response->getBody();
            $wallet = json_decode((string) $body);

            // check balance
            if($wallet->balance < 0){
                return false;
            }


        }catch(\Exception $ex){
            return false;
        }

        return true;
    }

    public function useWalletBak($user_id)
    {
        $user = User::where('id', $user_id)->first();
        $role = Role::whereId($user->role_id)->first();
        if ($role->name === User::MASTER) {
            return true;
        }
        $subscription_id = UserWallet::where('user_id', $user->id)->where('module_id', ShortUrl::MINIME)->first()->subscription_module_id;
        $Total_limit = UserSubscription::where('user_id', $user->id)->where('subscription_module_id', $subscription_id)->first()->total_units;
        $pre_use_balance = $user->wallets->where('module_id', ShortUrl::MINIME)->first()->balance;
        $post_use_balance = $pre_use_balance - 1;
        $before_uses_percentage = 100 - ($pre_use_balance * 100) / $Total_limit;
        $after_uses_percentage = 100 - ($post_use_balance * 100) / $Total_limit;

        $userWallet = UserWallet::whereUserId($user_id)->whereModuleId(ShortUrl::MINIME)->first();
        if ($userWallet->balance > 0) {
            $userWallet = UserWallet::whereUserId($user_id)->whereModuleId(ShortUrl::MINIME)->update(['balance' => --$userWallet->balance]);
            $this->chackPercentage($before_uses_percentage, $after_uses_percentage, $user->id, $Total_limit, $Total_limit - $post_use_balance);
        } else {
            $userWallet = 0;
        }

        if ($user_id == 0) {
            $user = auth()->user();
        } else {
            $user = \App\Models\User::where('id', $user_id)->first();
        }
        $package_status = true;
        $wallet_data = $user->wallets->where('module_id', ShortUrl::MINIME)->first();
        if ($wallet_data) {
            $user_subscriptions = \App\Models\Payment\UserSubscription::whereUserId($user->id)->whereSubscriptionModuleId($wallet_data->subscription_module_id)->first();
            if ($user_subscriptions->expired_at != null) {
                if (\Carbon\Carbon::parse($user_subscriptions->expired_at)->getTimestamp() <= \Carbon\Carbon::now()->getTimestamp()) {
                    Log::info('if expired date');
                    $userWallet = 0;
                }
            }
        } else {
            Log::info('wallet data' . $wallet_data . '-----------------------------------------');
            $userWallet = 0;
        }
        return $userWallet === 0 ? false: true;
    }

    public function chackPercentage($beforePercentage, $afterPercentage, $user_id, $Total_limit, $before_uses_percentage)
    {
        $Percentage = null;
        if ($beforePercentage < 100 && $afterPercentage == 100) {
            $Percentage = 100;
            notificationMails(ShortUrl::MINIME, $user_id);
        } else if ($beforePercentage < 95 && $afterPercentage >= 95) {
            //$Percentage = 95;
        } else if ($beforePercentage < 75 && $afterPercentage >= 75) {
            $Percentage = 75;
        } else if ($beforePercentage < 50 && $afterPercentage >= 50) {
            //$Percentage = 50;
        }
        if ($Percentage != null) {
            $modulesName = ShortUrl::APPNAME;
            $userEmail = User::where('id', $user_id)->with('subscription')->first();
            $email_send_to = $userEmail->email;

            try {
                $plan = $userEmail->subscription->title ?? '';
            } catch (\Exception $e) {
                $plan = '';
            }

            $data_email = [
                "email" => $userEmail->email,
                "name" => $userEmail->name,
                "appName" => $modulesName,
                "percentage" => $Percentage,
                "used" => $before_uses_percentage,
                "total" => $Total_limit,
                "plan" => $plan
            ];
            $sub = 'USAGE ALERT!  : MarketerMagic ' . $modulesName . ' Application';
            Mail::send('app_percentage', $data_email, function ($message) use ($email_send_to, $sub) {
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $message->to($email_send_to);
                $message->subject($sub);
            });
        }
        return;
    }

    function notificationMails($module_id = 0, $uesr_id = 0)
    {
        if ($module_id == 0 || $uesr_id == 0) {
            return;
        }
        $date = Carbon::today()->addDay(7);
        $notificationUser = NotificationMails::where('user_id', $uesr_id)->where('module_id', $module_id)->count();
        if ($notificationUser == 0) {
            NotificationMails::create([
                'user_id' => $uesr_id,
                'module_id' => $module_id,
                'next_notification_date' => $date
            ]);
        } else {
            NotificationMails::where('user_id', $uesr_id)->where('module_id', $module_id)
                ->update(['next_notification_date' => $date]);
        }
    }

    public function addhttp($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

    public function sendToAPI_server($broadcast)
    {

//        dd($response);
    }

    public function AddHttpToURL($string)
    {
        if (substr($string, 0, 7) == "http://") {
            return $string;
        } else if (substr($string, 0, 8) == "https://") {
            return $string;
        } else {
            return 'http://' . $string;
        }
    }
}
