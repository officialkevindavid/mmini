<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationMails extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function userData(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function moduleName(){
        return $this->hasOne(Module::class,'id','module_id');
    }
}
