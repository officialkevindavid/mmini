<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortUrl extends Model
{
    use SoftDeletes;

    const ACTIVE = 1, IN_ACTIVE = 0;
    CONST SHORTURL = 0, SPLITURL = 1, RETARGETING = 2, DEEPLINK = 3, LINKTREE = 4, TELNUMBER = 5 ;
    const SUCCESS = 1, FAIL = 0;
    const PERCENTAGE = 100;
    const DEFAULT_URL = 'http://www.marketermagic.com';
    const MINIME = 6;
    const APPNAME = 'MiniMe';

    protected $fillable = [
        'id',
        'user_id',
        'type',
        'short_url',
        'long_url_first',
        'long_url_second',
        'percentage',
    ];

    public function urlStatus(){
        return $this->hasMany(UrlStatus::class);
    }
    
    public function telnumber(){
        return $this->hasOne(Telnumber::class);
    }

}