<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <style type="text/css">
        /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }

        .text-center{
            text-align:center;
        }
        .headerclass {
            /* border: 1px solid #fff; */
            border-bottom: 3px solid #02CED1;
        }
        .ma-0{
            margin-top: 20px;
        }
        .contentcenterclass{
            padding-left: 25%;
            padding-right: 25%;
        }

        .contentcenterclass-2{
            padding-left:40%;
            padding-right:40%;
        }

        .ma-bot-10{
            margin-bottom: 5%;
        }

        .text-left{
            float:left;
        }

        .login-btn{
            background: linear-gradient(353.3deg, #B1F1A2 0%, #06CFD2 99.78%);
            box-shadow: 0px 10px 32px rgba(20, 209, 206, 0.391842);
            border-radius: 3px;
            font-style: normal;
            font-weight: bold;
            font-size: 16px;
            line-height: 22px;
            text-align: center;
            color: #FFFFFF;
            border: none;
            padding-top: 12px;
            padding-bottom: 12px;
            padding-left: 5%;
            padding-right: 5%;
        }

        hr{
            height: 1px;
            color: #e2d6d6;
            background-color: #e2d6d6;
            border: none;
        }

        .hr-ma-0{
            margin-top: 20px;
            margin-left: 20%;
            margin-right: 20%;
        }
        .fontweight{
            font-weight: 550;
        }

        /* Let's make sure all tables have defaults */
        table td {
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        body {
            background-color: #f6f6f6;
        }

        .body-wrap {
            background-color: #f6f6f6;
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 1080px !important;
            margin: 0 auto !important;
            clear: both !important;
        }

        .content {
            max-width: 1080px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #fff;
            border: 1px solid #e9e9e9;
            border-radius: 3px;
        }

        .main-header {
            background: #fff;
            border-radius: 3px;
        }

        .main-content {
            background: #fff;
            border-bottom: 1px solid #e9e9e9;
            border-radius: 3px;
        }


        .content-wrap {
            padding: 20px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        .header {
            width: 100%;
            margin-bottom: 20px;
        }

        .footer {
            width: 100%;
            clear: both;
            color: #999;
            padding: 20px;
        }
        .footer a {
            color: #999;
        }
        .footer p, .footer a, .footer unsubscribe, .footer td {
            font-size: 12px;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            color: #000;
            margin: 40px 0 0;
            line-height: 1.2;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
        }

        h2 {
            font-size: 24px;
        }

        h3 {
            font-size: 18px;
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        p, ul, ol {
            margin-bottom: 10px;
            font-weight: normal;
        }
        p li, ul li, ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* -------------------------------------
            LINKS & BUTTONS
        ------------------------------------- */
        a {
            color: #02CED1;
            text-decoration: underline;
        }

        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #1ab394;
            border: solid #1ab394;
            border-width: 5px 10px;
            line-height: 2;
            font-weight: bold;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 5px;
            text-transform: capitalize;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .aligncenter {
            text-align: center;
        }

        .alignright {
            text-align: right;
        }

        .alignleft {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        /* -------------------------------------
            ALERTS
            Change the class depending on warning email, good email or bad email
        ------------------------------------- */
        .alert {
            font-size: 16px;
            color: #fff;
            font-weight: 500;
            padding: 20px;
            text-align: center;
            border-radius: 3px 3px 0 0;
        }
        .alert a {
            color: #fff;
            text-decoration: none;
            font-weight: 500;
            font-size: 16px;
        }
        .alert.alert-warning {
            background: #f8ac59;
        }
        .alert.alert-bad {
            background: #ed5565;
        }
        .alert.alert-good {
            background: #1ab394;
        }

        /* -------------------------------------
            INVOICE
            Styles for the billing table
        ------------------------------------- */
        .invoice {
            margin: 40px auto;
            text-align: left;
            width: 80%;
        }
        .invoice td {
            padding: 5px 0;
        }
        .invoice .invoice-items {
            width: 100%;
        }
        .invoice .invoice-items td {
            border-top: #eee 1px solid;
        }
        .invoice .invoice-items .total td {
            border-top: 2px solid #333;
            border-bottom: 2px solid #333;
            font-weight: 700;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            h1, h2, h3, h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content, .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }

    </style>
</head>

<body>

<table style="background-color: #f6f6f6;width: 100%;">
    <tr>
        <td style="vertical-align: top;"></td>
        <td style="display: block !important;max-width: 1080px !important;margin: 0 auto !important;clear: both !important;vertical-align: top;" width="100%;">
            <div style="max-width: 1080px;margin: 0 auto;display: block;padding: 20px;">

            <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align:center;border-bottom: 3px solid #02CED1;vertical-align: top;">
                            <img src="{{ asset('images/logo2.png') }}" style="width:200px;height: 80px;max-height:80px;">
                        </td>
                    </tr>
            </table>    

                @yield('content')

                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="vertical-align: top;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;text-align:center;padding-left:40%;padding-right:40%;" class="contentcenterclass-2 text-center">
                           <table cellpadding="10" cellspacing="10" style="width:100%;">
                            <tr>
                                <td style="vertical-align: top;"><img src="{{ asset('images/facebook.png') }}"></td>
                                <td style="vertical-align: top;"><img src="{{ asset('images/twitter.png') }}"></td>
                                <td style="vertical-align: top;"><img src="{{ asset('images/instagram.png') }}"></td>
                                <td style="vertical-align: top;"><img src="{{ asset('images/youtube.png') }}"></td>
                            </tr>
                           </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <h3 class="ma-0 text-center" style="margin-top: 20px;text-align:center;font-family:Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;color: #000;margin: 40px 0 0;line-height: 1.2;font-weight: 550;font-size: 16px;">MarketerMagic</h3>
                            <p  style="margin-top: 20px;text-align:center;margin-bottom: 10px;font-weight: normal;color:#647579;" class="ma-0 text-center">© 2019 MarketerMagic. All Rights Reserved.</p>
                            <p  style="margin-top: 20px;text-align:center;margin-bottom: 10px;font-weight: normal;color:#647579;" class="ma-0 text-center contentcenterclass">You’ve receiving this because we want to help you get the most out of your new marketermagic account.</p>
                        </td>
                    </tr>
                </table>     
            </div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>