<?php

namespace App\Observers;

use App\Models\ShortUrl;
use App\Models\UrlStatus;

class UrlStatusObserver
{
    public function created(UrlStatus $urlStatus)
    {
        $shortUrlData = ShortUrl::where('id', $urlStatus->short_url_id)->first();
        $shortUrlData->click = $shortUrlData->click + 1;
        $shortUrlData->save();
    }

    public function updated(UrlStatus $urlStatus)
    {
    }

    public function deleted(UrlStatus $urlStatus)
    {
    }

    public function restored(UrlStatus $urlStatus)
    {
    }

    public function forceDeleted(UrlStatus $urlStatus)
    {
    }
}
