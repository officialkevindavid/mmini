<?php

namespace App\Models;

use App\Models\Payment\UserSubscription;
use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
    CONST RESET = 0;
    protected $table = 'user_wallets';
    protected $guarded = ['id'];

    public function userSubscription()
    {
        return $this->belongsTo(UserSubscription::class, 'subscription_module_id', 'subscription_module_id');
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
